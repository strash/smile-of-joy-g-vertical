extends Control


signal level1_pressed
signal level2_pressed


const page_tex_inactive: Resource = preload("res://assets/image/ic_page_inactive.png")
const page_tex_active: Resource = preload("res://assets/image/ic_page_active.png")

var dragged: bool = false # drag state
var offset: float = 30.0 # offset from the left edge of the screen
var scroll_acceleration: float = 0.0 # scroll speed when released
var time: float = 0.25 # for tweening
var page_index: int = 0 # current page
var max_pages: int # max pages
var page_width: float # page width
var paging_dot_size: Vector2


# BUILTINS - - - - - - - - -


func _ready() -> void:
	paging_dot_size = $Pages/Page0.rect_size
	max_pages = ($Swipe as Control).get_child_count()
	page_width = ($Swipe/BtnLvl1 as Button).rect_size.x + offset
	($Swipe as Control).rect_size.x = max_pages * page_width
	($Swipe as Control).rect_position.x = offset
	for i in max_pages:
		var child: Button = ($Swipe as Control).get_children()[i] as Button
		child.rect_position.x = child.rect_size.x * i + offset * i


# SCROLL
func _input(event: InputEvent) -> void:
	if event is InputEventScreenDrag:
		if event.relative != Vector2.ZERO:
			dragged = true
			scroll_acceleration = event.speed.x / 8.0
			($Swipe as Control).rect_position.x += event.relative.x
	elif event is InputEvent and Input.is_action_just_released("ui_mouse") and dragged:
		_scroll_down()
	elif event is InputEvent and event.is_pressed():
		dragged = false
		scroll_acceleration = 0.0


func _process(_delta: float) -> void:
	_set_page()


# METHODS - - - - - - - - -


# slow down after swipe in same direction
func _scroll_down() -> void:
	var _t: int
	var pos: float = ($Swipe as Control).rect_position.x
	var new_pos: float = pos + scroll_acceleration
	_t = ($Tween as Tween).interpolate_property(($Swipe as Control), "rect_position:x", pos, new_pos, time)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(time), "timeout")
		_scroll_to_page()


# scroll to page after slow down
func _scroll_to_page() -> void:
	var _t: int
	var pos: float = ($Swipe as Control).rect_position.x
	var new_pos: float = -(page_index * page_width) + offset
	_t = ($Tween as Tween).interpolate_property(($Swipe as Control), "rect_position:x", pos, new_pos, time / 2)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# set page and paging
func _set_page() -> void:
	var pos: float = ($Swipe as Control).rect_position.x
	page_index = round(abs(pos) / page_width) as int
	if page_index >= max_pages:
		page_index = max_pages - 1
	if pos > 0.0:
		page_index = 0
	var _children: Array = ($Pages as Control).get_children() as Array
	for i in ($Pages as Control).get_child_count():
		var _child: Control = _children[i] as Control
		var tex: TextureRect = _child.get_node("TextureRect") as TextureRect
		if i == page_index:
			tex.texture = page_tex_active
		else:
			tex.texture = page_tex_inactive
		tex.rect_size = Vector2.ZERO
		tex.rect_position = (paging_dot_size - tex.rect_size) / 2.0


# SIGNALS - - - - - - - - -


func _on_BtnLvl1_pressed() -> void:
	if not dragged:
		emit_signal("level1_pressed")


func _on_BtnLvl2_pressed() -> void:
	if not dragged:
		emit_signal("level2_pressed")
